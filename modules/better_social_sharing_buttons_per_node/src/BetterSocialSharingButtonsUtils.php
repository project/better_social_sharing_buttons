<?php

namespace Drupal\better_social_sharing_buttons_per_node;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;

/**
 * Various utilities method for better_social_sharing_buttons_per_node.
 */
class BetterSocialSharingButtonsUtils {

  /**
   * Get the sharing buttons display state of a node.
   */
  public static function isNodeSharingActive(
    NodeInterface $node,
    $sharing_buttons_active_default = TRUE,
  ): bool {
    // If no value has been set on the node, use the default value.
    if ($node->hasField('better_social_sharing_buttons_active')) {
      $active_field = $node->get('better_social_sharing_buttons_active');
      if ($active_field->isEmpty()) {
        // The field is empty, use default value.
        $sharing_buttons_active = $sharing_buttons_active_default;
      }
      else {
        $sharing_buttons_active = $active_field->value;
      }
    }
    else {
      // Field doesn't exist, use default value.
      $sharing_buttons_active = $sharing_buttons_active_default;
    }
    return $sharing_buttons_active;
  }

  /**
   * Check if user is authorized to update sharing display state.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The user to check permissions for.
   *
   * @return bool
   *   TRUE if the user can change buttons display, FALSE otherwise.
   */
  public static function hasEditSharingPermission(AccountProxyInterface $currentUser): bool {
    return $currentUser->hasPermission('edit social sharing buttons node display') ||
      $currentUser->hasPermission('administer nodes');
  }

}
