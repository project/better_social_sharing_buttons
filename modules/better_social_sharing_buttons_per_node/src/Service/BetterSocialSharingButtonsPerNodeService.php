<?php

namespace Drupal\better_social_sharing_buttons_per_node\Service;

use Drupal\better_social_sharing_buttons\Service\BetterSocialSharingButtonsService;
use Drupal\better_social_sharing_buttons_per_node\BetterSocialSharingButtonsUtils;
use Drupal\node\NodeInterface;

/**
 * Service for handling social sharing buttons.
 */
class BetterSocialSharingButtonsPerNodeService extends BetterSocialSharingButtonsService {

  /**
   * {@inheritDoc}
   */
  public function entityExtraFieldInfo(): ?array {
    if ($this->config->get('node_field')) {
      $extra = [];
      /** @var \Drupal\node\NodeTypeInterface $node_type */
      foreach ($this->getNodeTypes() as $node_type) {
        if ($node_type->getThirdPartySetting(
          'better_social_sharing_buttons_per_node', 'active', 0)) {
          $extra['node'][$node_type->Id()]['display']['sharing_buttons'] = [
            'label' => $this->t('Better Social Sharing Buttons'),
            'description' => '',
            'weight' => 100,
            'visible' => TRUE,
          ];
        }
      }

      return $extra;
    }
    return NULL;
  }

  /**
   * Check if sharing buttons should be displayed on this node.
   */
  protected function isSharingButtonsActive(NodeInterface $node): bool {
    /** @var \Drupal\node\NodeTypeInterface $node_type */
    $node_type = $this->getNodeType($node->bundle());

    $sharing_buttons_active = $node_type->getThirdPartySetting(
      'better_social_sharing_buttons_per_node', 'active', 0);
    if (!$sharing_buttons_active) {
      // Better social sharing buttons has not been enabled for this node type.
      return FALSE;
    }

    $sharing_buttons_override = $node_type->getThirdPartySetting(
      'better_social_sharing_buttons_per_node', 'override', 0);
    if (!$sharing_buttons_override) {
      // If not overridden at the node level, display the buttons by default.
      return TRUE;
    }

    $sharing_buttons_override_default = $node_type->getThirdPartySetting(
      'better_social_sharing_buttons_per_node', 'override_default', 1);

    return BetterSocialSharingButtonsUtils::isNodeSharingActive(
      $node, $sharing_buttons_override_default);
  }

}
