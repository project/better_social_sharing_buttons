<?php

namespace Drupal\better_social_sharing_buttons\Service;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\node\NodeInterface;

/**
 * Service for handling the social sharing buttons.
 */
interface BetterSocialSharingButtonsServiceInterface {

  /**
   * Returns the extra field info array.
   */
  public function entityExtraFieldInfo(): ?array;

  /**
   * Display the social sharing buttons on node view.
   */
  public function nodeView(array &$build, NodeInterface $node, EntityViewDisplayInterface $display): void;

}
