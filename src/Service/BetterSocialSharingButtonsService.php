<?php

namespace Drupal\better_social_sharing_buttons\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

/**
 * Service for handling social sharing buttons.
 */
class BetterSocialSharingButtonsService implements BetterSocialSharingButtonsServiceInterface {

  use StringTranslationTrait;

  /**
   * The Better Social Sharing Buttons module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new BetterSocialSharingButtonsService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->config = $config_factory->get('better_social_sharing_buttons.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the node type storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The node type storage.
   */
  private function getNodeTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('node_type');
  }

  /**
   * Get node type entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The array of node type entity objects indexed by their IDs.
   */
  protected function getNodeType(string $bundle): EntityInterface {
    return $this->getNodeTypeStorage()->load($bundle);
  }

  /**
   * Get node type entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The array of node type entity objects indexed by their IDs.
   */
  protected function getNodeTypes(): array {
    return $this->getNodeTypeStorage()->loadMultiple();
  }

  /**
   * {@inheritDoc}
   */
  public function entityExtraFieldInfo(): ?array {
    if ($this->config->get('node_field')) {
      $extra = [];
      foreach ($this->getNodeTypes() as $bundle) {
        $extra['node'][$bundle->Id()]['display']['sharing_buttons'] = [
          'label' => $this->t('Better Social Sharing Buttons'),
          'description' => '',
          'weight' => 100,
          'visible' => TRUE,
        ];
      }
      return $extra;
    }
    return NULL;
  }

  /**
   * Check if sharing buttons should be displayed on this node.
   */
  protected function isSharingButtonsActive(NodeInterface $node): bool {
    // We always display sharing buttons by default.
    // The per node service overrides this method.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function nodeView(array &$build, NodeInterface $node, EntityViewDisplayInterface $display): void {
    if ($display->getComponent('sharing_buttons') && $this->isSharingButtonsActive($node)) {

      $items = [];
      global $base_url;

      // It is impossible to generate canonical absolute URL for the entity
      // without ID - it will trigger EntityMalformedException with the
      // following message: "The entity cannot have a URI as it does not have an
      // ID", so instead we use website base URL to render buttons during
      // preview, as this is the only time when entity could be rendered without
      // being saved and have an ID.
      $items['page_url'] = $node->id() ? $node->toUrl('canonical', ['absolute' => TRUE]) : $base_url;
      $items['description'] = '';
      $items['title'] = $node->get('title')->value;
      $items['width'] = $this->config->get('width');
      $items['radius'] = $this->config->get('radius');
      $items['facebook_app_id'] = $this->config->get('facebook_app_id');
      $items['print_css'] = $this->config->get('print_css');
      $items['iconset'] = $this->config->get('iconset');
      $items['services'] = $this->config->get('services');
      $items['base_url'] = $base_url;

      $build['sharing_buttons'] = [
        '#theme' => 'better_social_sharing_buttons',
        '#items' => $items,
      ];
    }
  }

}
